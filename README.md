# Some ugly scripts to get data from Valmyndigheten

* run.py downloads pages from Valmyndigheten
* run_csv.py extracts tables from the data
* clean.R cleans things up a bit

dependencies are in requirements.txt and can be installed by

pip install -r requirements.txt
